/* ESS Linac Simulator */
/* By Emanuele Laface - Emanuele.Laface@esss.se */
/* Last update July 3rd 2013 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_sf_ellint.h>
#include <time.h>

#define speed_of_light 299792458
/* #define R0 1.53469825003334879e-18 */ /* for protons */
#define R0 1.53473466646713727192e-18 /* for H- */
/*#define mass 938.272046 */ /* Proton */
#define mass 938.29431 /* H- */
#define charge 1.602176565e-19

#define E_COEFF 11.18033988749894848204 /* 5^1.5 */

#define SHOWTIME 0

struct machine_element
{
    int index; /* the index of the machine's element */
    double start_position; /* the s coordinate of the starting point of the element. It is in meters */
    double end_position; /* the s coordinate of the ending point of the element. It is in meters */
    double matrix[6][6]; /* the transport matrix for the coordinate x, x', y, y', z, z' */
    double optics[9][9]; /* the transport matrix for the optical functions beta, alfa, gamma */
    double determinant[3]; /* the three determinants of the 2x2 blocks of matrix */
    double aperture; /* the geometrical aperture of the element, assuming it circular */
    double energy; /* the energy at the position of the element */
    double beta; /* the relativistic beta at the position of the element */
    double gamma; /* the relativistic gamma at the position of the element */
    double frequency; /* the frequency of the RF */
    double space_charge; /* the coefficient of the space charghe that does not depend on the geometry of the beam */
};

struct optical_functions
{
    double in[9];
    double out[9];
};

struct optical_functions envelope;
struct machine_element *machine;
int number_of_elements;
int number_of_particles;

double x_natural_emittance;
double x_normalized_emittance;
double y_natural_emittance;
double y_normalized_emittance;
double z_natural_emittance;
double z_normalized_emittance;


/* the next function transform the transport matrix of the coordinates in the transport matrix of the optical functions
 * it is important to remember that at the end of the transformation the matrix must be divided for the determinant
 * of the transport matrix. This is to compensate the deformation of the phase-space due to the acceleration of the
 * particles.
 */

void transfer2optics_matrix(int element_index)
{
    int i,j;

    for (i=0;i<9;i++)
        for (j=0;j<9;j++)
            machine[element_index].optics[i][j]=0;

    machine[element_index].optics[0][0]=machine[element_index].matrix[0][0]*machine[element_index].matrix[0][0];
    machine[element_index].optics[0][1]=-2.0*machine[element_index].matrix[0][0]*machine[element_index].matrix[0][1];
    machine[element_index].optics[0][2]=machine[element_index].matrix[0][1]*machine[element_index].matrix[0][1];
    machine[element_index].optics[1][0]=-machine[element_index].matrix[0][0]*machine[element_index].matrix[1][0];
    machine[element_index].optics[1][1]=machine[element_index].matrix[0][0]*machine[element_index].matrix[1][1]+machine[element_index].matrix[0][1]*machine[element_index].matrix[1][0];
    machine[element_index].optics[1][2]=-machine[element_index].matrix[0][1]*machine[element_index].matrix[1][1];
    machine[element_index].optics[2][0]=machine[element_index].matrix[1][0]*machine[element_index].matrix[1][0];
    machine[element_index].optics[2][1]=-2.0*machine[element_index].matrix[1][0]*machine[element_index].matrix[1][1];
    machine[element_index].optics[2][2]=machine[element_index].matrix[1][1]*machine[element_index].matrix[1][1];

    machine[element_index].optics[3][3]=machine[element_index].matrix[2][2]*machine[element_index].matrix[2][2];
    machine[element_index].optics[3][4]=-2.0*machine[element_index].matrix[2][2]*machine[element_index].matrix[2][3];
    machine[element_index].optics[3][5]=machine[element_index].matrix[2][3]*machine[element_index].matrix[2][3];
    machine[element_index].optics[4][3]=-machine[element_index].matrix[2][2]*machine[element_index].matrix[3][2];
    machine[element_index].optics[4][4]=machine[element_index].matrix[2][2]*machine[element_index].matrix[3][3]+machine[element_index].matrix[2][3]*machine[element_index].matrix[3][2];
    machine[element_index].optics[4][5]=-machine[element_index].matrix[2][3]*machine[element_index].matrix[3][3];
    machine[element_index].optics[5][3]=machine[element_index].matrix[3][2]*machine[element_index].matrix[3][2];
    machine[element_index].optics[5][4]=-2.0*machine[element_index].matrix[3][2]*machine[element_index].matrix[3][3];
    machine[element_index].optics[5][5]=machine[element_index].matrix[3][3]*machine[element_index].matrix[3][3];

    machine[element_index].optics[6][6]=machine[element_index].matrix[4][4]*machine[element_index].matrix[4][4];
    machine[element_index].optics[6][7]=-2.0*machine[element_index].matrix[4][4]*machine[element_index].matrix[4][5];
    machine[element_index].optics[6][8]=machine[element_index].matrix[4][5]*machine[element_index].matrix[4][5];
    machine[element_index].optics[7][6]=-machine[element_index].matrix[4][4]*machine[element_index].matrix[5][4];
    machine[element_index].optics[7][7]=machine[element_index].matrix[4][4]*machine[element_index].matrix[5][5]+machine[element_index].matrix[4][5]*machine[element_index].matrix[5][4];
    machine[element_index].optics[7][8]=-machine[element_index].matrix[4][5]*machine[element_index].matrix[5][5];
    machine[element_index].optics[8][6]=machine[element_index].matrix[5][4]*machine[element_index].matrix[5][4];
    machine[element_index].optics[8][7]=-2.0*machine[element_index].matrix[5][4]*machine[element_index].matrix[5][5];
    machine[element_index].optics[8][8]=machine[element_index].matrix[5][5]*machine[element_index].matrix[5][5];

    for (i=0;i<9;i++)
        for (j=0;j<9;j++)
            machine[element_index].optics[i][j]=machine[element_index].optics[i][j]/machine[element_index].determinant[0];
    return;
}


/* the next function initialize the machine elements according to the files of matrices and the file of aperture and energy */

int init_machine(char *matrix_filename, char *aperture_energy_filename)
{
    FILE *matrix_file;
    FILE *aperture_energy_file;
    char buffer[256];
    int i,j,k;

    if ( (matrix_file=fopen(matrix_filename,"r")) == NULL )
    {       
        printf ("Error opening the matrix file\n");
        return 1;
    }
    if ( (aperture_energy_file=fopen(aperture_energy_filename,"r")) == NULL )
    {       
        printf ("Error opening the aperture and energy file\n");
        return 1;
    }

    i=0;
    while(fgets(buffer,sizeof(buffer),matrix_file))
        i++;
    rewind(matrix_file);

    /* the block of a matrix contains the number of the element, the starting and ending coordinates and 6x6 entries.
     * This is the reason why the number of elements is the number of lines divided by 8 */

    number_of_elements=i/8;

    machine=(struct machine_element *)malloc(number_of_elements*sizeof(struct machine_element));

    for (i=0;i<number_of_elements;i++)
    {
        if (SHOWTIME==1)
            printf("Loading Matrix File: %.2f%%\r",(double)i/(double)number_of_elements*100);

        /* fill the matrix elements */
        fgets(buffer,sizeof(buffer),matrix_file);
        machine[i].index=i;
        fscanf(matrix_file,"From postion %lf m to %lf m",&machine[i].start_position,&machine[i].end_position);
        fgets(buffer,sizeof(buffer),matrix_file);
        for (j=0;j<6;j++)
            for(k=0;k<6;k++)
                fscanf(matrix_file,"%lf",&machine[i].matrix[j][k]);

        /* calculate the three determinants */
        machine[i].determinant[0]=machine[i].matrix[0][0]*machine[i].matrix[1][1]-machine[i].matrix[0][1]*machine[i].matrix[1][0];
        machine[i].determinant[1]=machine[i].matrix[2][2]*machine[i].matrix[3][3]-machine[i].matrix[2][3]*machine[i].matrix[3][2];
        machine[i].determinant[2]=machine[i].matrix[4][4]*machine[i].matrix[5][5]-machine[i].matrix[4][5]*machine[i].matrix[5][4];

        /* calculate the transport matrices for the optical functions */
        transfer2optics_matrix(i);
        fscanf(matrix_file,"%lf %lf",&machine[i].start_position,&machine[i].end_position);
        fscanf(aperture_energy_file,"%lf %lf %lf %lf",&machine[i].end_position, &machine[i].aperture,&machine[i].energy,&machine[i].frequency);
        machine[i].aperture=machine[i].aperture/1000; /* from TraceWin the aperture is in mm, shit! */
        machine[i].gamma=1+machine[i].energy/mass; /* calculate the relativistic gamma */
        machine[i].beta=sqrt(1-1/pow(machine[i].gamma,2)); /* calculate the relativistic beta */

        /* this is the space charge coefficient that does not depend from the geometry, what others call perveance but in this case is multiplied for the step length */
        machine[i].space_charge=R0/charge/machine[i].frequency/pow(machine[i].beta,2)/pow(machine[i].gamma,3); /* this is for elliptical bunches */

    }
    if (SHOWTIME==1)
        printf("\ndone.\n");

    fclose(matrix_file);
    fclose(aperture_energy_file);

    return 0;
}

int init_bunch(char *filename)
{
    FILE *init_file;
    char buffer[256];
    int i;
    double current;
    double duty_cycle;
    double kinetic_energy;
    double alfa_x;
    double beta_x;
    double alfa_y;
    double beta_y;
    double alfa_z;
    double beta_z;

    if ( (init_file=fopen(filename,"r")) == NULL )
    {       
        printf ("Error opening the init file\n");
        return 1;
    }

    for (i=0;i<14;i++)
        fgets(buffer,sizeof(buffer),init_file);
    fscanf(init_file,"%d",&number_of_particles);
    fscanf(init_file,"%lf",&current);
    fscanf(init_file,"%lf",&duty_cycle);
    fscanf(init_file,"%lf",&x_normalized_emittance);
    fscanf(init_file,"%lf",&y_normalized_emittance);
    fscanf(init_file,"%lf",&z_normalized_emittance);
    fscanf(init_file,"%lf",&kinetic_energy);
    fscanf(init_file,"%lf",&alfa_x);
    fscanf(init_file,"%lf",&beta_x);
    fscanf(init_file,"%lf",&alfa_y);
    fscanf(init_file,"%lf",&beta_y);
    fscanf(init_file,"%lf",&alfa_z);
    fscanf(init_file,"%lf",&beta_z);

    /* This works with TraceWin, but why??????????? */
    beta_z=beta_z/pow(machine[0].gamma,2); 

    envelope.in[0]=beta_x;
    envelope.in[1]=alfa_x;
    envelope.in[2]=(1+pow(alfa_x,2))/beta_x;
    envelope.in[3]=beta_y;
    envelope.in[4]=alfa_y;
    envelope.in[5]=(1+pow(alfa_y,2))/beta_y;
    envelope.in[6]=beta_z;
    envelope.in[7]=alfa_z;
    envelope.in[8]=(1+pow(alfa_z,2))/beta_z;

    for (i=0;i<9;i++)
        envelope.out[i]=0;

    fclose(init_file);
    for (i=0;i<number_of_elements;i++)
        machine[i].space_charge=current*machine[i].space_charge;

    x_natural_emittance=x_normalized_emittance/(machine[0].gamma*machine[0].beta);
    y_natural_emittance=y_normalized_emittance/(machine[0].gamma*machine[0].beta);
    z_natural_emittance=z_normalized_emittance/(machine[0].gamma*machine[0].beta);

    return 0;
}

void envelope_tracking(int element_index)
{
    int i,j;

    for (i=0;i<9;i++)
        for (j=0;j<9;j++)
            envelope.out[i]=envelope.out[i]+machine[element_index].optics[i][j]*envelope.in[j];
    return;
}

void new_step_envelope()
{
    int i;
    for(i=0;i<9;i++)
    {
        envelope.in[i]=envelope.out[i];
        envelope.out[i]=0;
    }
    return;
}

void space_charge_tracking(int element_index)
{
    double int_x, int_y, int_z;
    double sigma_x,sigma_y,sigma_z;

    sigma_x=envelope.in[0]*x_natural_emittance;
    sigma_y=envelope.in[3]*y_natural_emittance;
    sigma_z=envelope.in[6]*z_natural_emittance*pow(machine[element_index].gamma,2);

    new_step_envelope();

    int_x=gsl_sf_ellint_RD(sigma_y,sigma_z,sigma_x,GSL_PREC_DOUBLE)/E_COEFF;
    int_y=gsl_sf_ellint_RD(sigma_z,sigma_x,sigma_y,GSL_PREC_DOUBLE)/E_COEFF;
    int_z=gsl_sf_ellint_RD(sigma_x,sigma_y,sigma_z,GSL_PREC_DOUBLE)/E_COEFF;

    int_x=machine[element_index].gamma*machine[element_index].space_charge*int_x*(machine[element_index+1].end_position-machine[element_index].start_position);
    int_y=machine[element_index].gamma*machine[element_index].space_charge*int_y*(machine[element_index+1].end_position-machine[element_index].start_position);
    int_z=machine[element_index].gamma*machine[element_index].space_charge*int_z*(machine[element_index+1].end_position-machine[element_index].start_position);

    envelope.out[0]=envelope.in[0];
    envelope.out[1]=-int_x*envelope.in[0]+envelope.in[1];
    envelope.out[2]=pow(int_x,2)*envelope.in[0]-2*int_x*envelope.in[1]+envelope.in[2];

    envelope.out[3]=envelope.in[3];
    envelope.out[4]=-int_y*envelope.in[3]+envelope.in[4];
    envelope.out[5]=pow(int_y,2)*envelope.in[3]-2*int_y*envelope.in[4]+envelope.in[5];

    envelope.out[6]=envelope.in[6];
    envelope.out[7]=-int_z*envelope.in[6]+envelope.in[7];
    envelope.out[8]=pow(int_z,2)*envelope.in[6]-2*int_z*envelope.in[7]+envelope.in[8];

    return;
}

int main(int argc, char **argv)
{
    int i;

    FILE *outfile_envelope;
    time_t start_time= time(0);
    double percentage_done;
    double time_remaining;
    double hours_remaining;
    double minutes_remaining;
    double seconds_remaining;

    if (init_machine(argv[1],argv[2])!=0)
        return 1;
    if (init_bunch(argv[3])!=0)
        return 1;
    outfile_envelope=fopen("out_envelope","w");

    percentage_done=0;
    time_remaining=0;
    hours_remaining=0;
    minutes_remaining=0;
    seconds_remaining=0;

    for (i=0;i<number_of_elements;i++)
    {
        if (SHOWTIME==1)
        {
            percentage_done=(double)i/(double)number_of_elements*100;
            time_remaining=(100-percentage_done)/(percentage_done/difftime( time(0), start_time));
            hours_remaining=floor(time_remaining/3600);
            minutes_remaining=floor((time_remaining-hours_remaining*3600)/60);
            seconds_remaining=floor(time_remaining-3600*hours_remaining-60*minutes_remaining);

            printf("Simulating: %.2f%%, ETA: %d:%d:%d                                        \r",percentage_done, (int)(hours_remaining), (int)(minutes_remaining), (int)(seconds_remaining));
        }

        if (i%2==0)
        {
            envelope_tracking(i);
            space_charge_tracking(i);
            new_step_envelope();
        }
        else
        {
            envelope_tracking(i);
            new_step_envelope();
        }

        fprintf(outfile_envelope,"%E %E %E %E ",machine[i].end_position, sqrt(envelope.in[0]*x_normalized_emittance/machine[i].beta/machine[i].gamma), sqrt(envelope.in[3]*y_normalized_emittance/machine[i].beta/machine[i].gamma), sqrt(envelope.in[6]*z_normalized_emittance/machine[i].beta/machine[i].gamma)); 
        fprintf(outfile_envelope,"%E %E %E\n", envelope.in[0], envelope.in[3], envelope.in[6]); 

    }
    if (SHOWTIME==1)
        printf("\ndone.\n");

    fclose(outfile_envelope);

    return 0;
}
