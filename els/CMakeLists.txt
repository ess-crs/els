add_executable(els els.c)
include_directories(${GSL_INCLUDE_DIR})
target_link_libraries(els ${GSL_LIBRARIES})
