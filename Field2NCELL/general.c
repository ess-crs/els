#include <stdio.h>
#include <stdlib.h>
#include "general.h"

int readFile(char *filename, cavity * cav)
{
    int i;
    FILE *cavityFile;
    if ( (cavityFile=fopen(filename,"r")) == NULL)
    {
        printf("Error opening file %s\n", filename);
        return -1;
    }
    fscanf(cavityFile, "%d %lf", &cav->NumberOfElements, &cav->Length);
    fscanf(cavityFile, "%lf", &cav->ScaleFactor);

    cav->NumberOfElements = cav->NumberOfElements - 1;

    cav->Field=(double *)malloc(sizeof(double)*cav->NumberOfElements);
    cav->AnalyticField=(double *)malloc(sizeof(double)*cav->NumberOfElements);
    cav->Position=(double *)malloc(sizeof(double)*cav->NumberOfElements);
    for (i=0;i<cav->NumberOfElements;i++)
    {
        fscanf(cavityFile, "%lf", &cav->Field[i]);
        cav->Field[i]=cav->Field[i]*cav->ScaleFactor;
        cav->Position[i]=(double)i/(double)cav->NumberOfElements*cav->Length;
    }

    fclose(cavityFile);
    return 0;
}
