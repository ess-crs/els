#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <gsl/gsl_multifit.h>
#include <gsl/gsl_fit.h>
#include "general.h"
#include "fit.h"

int sign(double value)
{
    if (value >0)
        return 1;
    if (value<0)
        return -1;
    return 0;
}

void exponentialFitLeft(cavity * cav)
{
    int i;
    int j,k,l;
    double xFirstPoint;
    double yFirstPoint;
    double xSecondPoint;
    double ySecondPoint;
    double tmpExponent;
    double tmpSigma;
    double error;
    double minError;

    i=0;
    while (fabs(cav->Field[i]) <= fabs(cav->Field[i+1]))
        i++;

    cav->ExpLeft.startIndex=0;
    cav->ExpLeft.endIndex=i;
    cav->ExpLeft.zero=cav->Position[cav->ExpLeft.endIndex];
    cav->ExpLeft.amplitude=cav->Field[cav->ExpLeft.endIndex];

    minError=1e100;

    for (j=cav->ExpLeft.endIndex-1; j>cav->ExpLeft.endIndex/2; j--)
        for (k=cav->ExpLeft.endIndex-1; k>cav->ExpLeft.endIndex/2; k--)
        {
            tmpExponent=0;
            tmpSigma=0;
            error=0;
            yFirstPoint=log(cav->Field[j]/cav->ExpLeft.amplitude);
            xFirstPoint=cav->Position[j]-cav->ExpLeft.zero;
            ySecondPoint=log(cav->Field[k]/cav->ExpLeft.amplitude);
            xSecondPoint=cav->Position[k]-cav->ExpLeft.zero;

            tmpExponent=log(yFirstPoint/ySecondPoint)/log(xFirstPoint/xSecondPoint); 
            tmpSigma=fabs(xFirstPoint/pow(fabs(yFirstPoint),1.0/tmpExponent));

            for (l=0;l<i;l++)
                error+=pow(cav->Field[l]-cav->ExpLeft.amplitude*exp(-pow(fabs(cav->Position[l]-cav->ExpLeft.zero)/tmpSigma, tmpExponent)),2);

            if (error<minError)
            {
                minError=error;
                cav->ExpLeft.exponent=tmpExponent;
                cav->ExpLeft.sigma=tmpSigma;
            }
        }
    for (i=cav->ExpLeft.startIndex;i<cav->ExpLeft.endIndex;i++)
        cav->AnalyticField[i]=cav->ExpLeft.amplitude*exp(-pow(fabs(cav->Position[i]-cav->ExpLeft.zero)/cav->ExpLeft.sigma, cav->ExpLeft.exponent));

    return;
}
/*
   void exponentialFitLeft(cavity * cav)
   {
   double *x;
   double *y;
   double covariant[3];
   double chi;
   double c0, c1;
   int numberOfData;
   int i;

   i=0;
   while (fabs(cav->Field[i]) <= fabs(cav->Field[i+1]))
   i++;

   cav->ExpLeft.endIndex=i;

   while (fabs(cav->Field[i]) > fabs(cav->Field[cav->ExpLeft.endIndex])/1.0e1)
   i--;

   cav->ExpLeft.startIndex=i;

   cav->ExpLeft.zero=cav->Position[cav->ExpLeft.endIndex];
   cav->ExpLeft.amplitude=cav->Field[cav->ExpLeft.endIndex];

   numberOfData=cav->ExpLeft.endIndex-cav->ExpLeft.startIndex;

   x=(double *)malloc(sizeof(double)*numberOfData);
   y=(double *)malloc(sizeof(double)*numberOfData);

   for (i=cav->ExpLeft.startIndex;i<cav->ExpLeft.endIndex; i++)
   {
   y[i-cav->ExpLeft.startIndex] = log(-log(cav->Field[i]/cav->ExpLeft.amplitude));
   x[i-cav->ExpLeft.startIndex] = log(fabs(cav->Position[i]-cav->ExpLeft.zero));
   }

   gsl_fit_linear(x, 1, y, 1, numberOfData, &c0, &c1, &covariant[0], &covariant[1], &covariant[2], &chi);

   cav->ExpLeft.exponent=c1;
   cav->ExpLeft.sigma=exp(-c0/cav->ExpLeft.exponent);
   cav->ExpLeft.startIndex=0;

   for (i=cav->ExpLeft.startIndex;i<cav->ExpLeft.endIndex;i++)
   cav->AnalyticField[i]=cav->ExpLeft.amplitude*exp(-pow(fabs(cav->Position[i]-cav->ExpLeft.zero)/cav->ExpLeft.sigma, cav->ExpLeft.exponent));

   return;
   }
   */
void exponentialFitRight(cavity * cav)
{
    int i;
    int j,k,l;
    double xFirstPoint;
    double yFirstPoint;
    double xSecondPoint;
    double ySecondPoint;
    double tmpExponent;
    double tmpSigma;
    double error;
    double minError;

    i=cav->NumberOfElements;
    while (fabs(cav->Field[i]) <= fabs(cav->Field[i-1]))
        i--;

    cav->ExpRight.startIndex=i;
    cav->ExpRight.endIndex=cav->NumberOfElements;
    cav->ExpRight.zero=cav->Position[cav->ExpRight.startIndex];
    cav->ExpRight.amplitude=cav->Field[cav->ExpRight.startIndex];

    minError=1e100;

    for (j=cav->ExpRight.startIndex; j<(cav->ExpRight.endIndex+cav->ExpRight.startIndex)/2; j++)
        for (k=cav->ExpRight.startIndex; k<(cav->ExpRight.endIndex+cav->ExpRight.startIndex)/2; k++)
        {
            tmpExponent=0;
            tmpSigma=0;
            error=0;
            yFirstPoint=log(cav->Field[j]/cav->ExpRight.amplitude);
            xFirstPoint=cav->Position[j]-cav->ExpRight.zero;
            ySecondPoint=log(cav->Field[k]/cav->ExpRight.amplitude);
            xSecondPoint=cav->Position[k]-cav->ExpRight.zero;

            tmpExponent=log(yFirstPoint/ySecondPoint)/log(xFirstPoint/xSecondPoint); 
            tmpSigma=fabs(xFirstPoint/pow(fabs(yFirstPoint),1.0/tmpExponent));

            for (l=i;l<cav->NumberOfElements;l++)
                error+=pow(cav->Field[l]-cav->ExpRight.amplitude*exp(-pow(fabs(cav->Position[l]-cav->ExpRight.zero)/tmpSigma, tmpExponent)),2);

            if (error<minError)
            {
                minError=error;
                cav->ExpRight.exponent=tmpExponent;
                cav->ExpRight.sigma=tmpSigma;
            }
        }

    for (i=cav->ExpRight.startIndex;i<cav->ExpRight.endIndex;i++)
        cav->AnalyticField[i]=cav->ExpRight.amplitude*exp(-pow(fabs(cav->Position[i]-cav->ExpRight.zero)/cav->ExpRight.sigma, cav->ExpRight.exponent));

    return;
}

void sinusoidalFitLeft(cavity * cav)
{
    gsl_matrix *x, *covariant;  
    gsl_vector *y;
    gsl_multifit_linear_workspace *workSpace;
    gsl_vector *fitCoefficients;
    double chi;
    /*  double realStart, realEnd; */
    int numberOfData;
    int i,j;

    cav->CosLeft.numberOfHarmonics=HARMONICS;

    cav->CosLeft.startIndex=cav->ExpLeft.endIndex;
    i=cav->CosLeft.startIndex;
    while (sign(cav->Field[i])==sign(cav->Field[i+1]))
        i++;
    cav->CosLeft.endIndex=i;

    numberOfData=cav->CosLeft.endIndex-cav->CosLeft.startIndex;
    /*
       realStart=cav->Position[cav->CosLeft.startIndex+1]-cav->Field[cav->CosLeft.startIndex+1]*(cav->Position[cav->CosLeft.startIndex+1]-cav->Position[cav->CosLeft.startIndex])/(cav->Field[cav->CosLeft.startIndex+1]-cav->Field[cav->CosLeft.startIndex]);
       realEnd=cav->Position[i+1]-cav->Field[i+1]*(cav->Position[i+1]-cav->Position[i])/(cav->Field[i+1]-cav->Field[i]);
       */

    cav->CosLeft.frequency=1.0/(2.0*(cav->Position[cav->CosLeft.endIndex]-cav->Position[cav->CosLeft.startIndex]));
    cav->CosLeft.phase=cav->Position[cav->CosLeft.startIndex];
    /*
       cav->CosLeft.frequency=1.0/(2.0*(realEnd-realStart));
       cav->CosLeft.phase=realStart;
       */
    x=gsl_matrix_alloc(numberOfData, cav->CosLeft.numberOfHarmonics);
    fitCoefficients=gsl_vector_alloc(cav->CosLeft.numberOfHarmonics);
    y=gsl_vector_alloc(numberOfData);
    covariant=gsl_matrix_alloc(cav->CosLeft.numberOfHarmonics, cav->CosLeft.numberOfHarmonics);
    workSpace=gsl_multifit_linear_alloc(numberOfData, cav->CosLeft.numberOfHarmonics);

    for (i=0;i<numberOfData; i++)
    {
        gsl_vector_set(y, i, cav->Field[i+cav->CosLeft.startIndex]);
        for (j=0;j<cav->CosLeft.numberOfHarmonics; j++)
            gsl_matrix_set(x, i, j, cos((double)(2*j+1)*M_PI*cav->CosLeft.frequency*(cav->Position[i+cav->CosLeft.startIndex]-cav->CosLeft.phase)));
    }

    gsl_multifit_linear(x, y, fitCoefficients, covariant, &chi, workSpace);

    cav->CosLeft.amplitude=(double *)malloc(sizeof(double)*cav->CosLeft.numberOfHarmonics);

    for (i=0;i<cav->CosLeft.numberOfHarmonics;i++)
        cav->CosLeft.amplitude[i]=gsl_vector_get(fitCoefficients, i);

    for (i=cav->CosLeft.startIndex;i<cav->CosLeft.endIndex;i++)
        cav->AnalyticField[i]=cav->CosLeft.amplitude[0]*cos(M_PI*cav->CosLeft.frequency*(cav->Position[i]-cav->CosLeft.phase))+cav->CosLeft.amplitude[1]*cos(3.0*M_PI*cav->CosLeft.frequency*(cav->Position[i]-cav->CosLeft.phase))+cav->CosLeft.amplitude[2]*cos(5.0*M_PI*cav->CosLeft.frequency*(cav->Position[i]-cav->CosLeft.phase));

    gsl_vector_free(y);
    gsl_vector_free(fitCoefficients);
    gsl_matrix_free(x);
    gsl_matrix_free(covariant);
    gsl_multifit_linear_free(workSpace);
    return;
}

void sinusoidalFitRight(cavity * cav)
{
    gsl_matrix *x, *covariant;  
    gsl_vector *y;
    gsl_multifit_linear_workspace *workSpace;
    gsl_vector *fitCoefficients;
    double chi;
    int numberOfData;
    int i,j;

    cav->CosRight.numberOfHarmonics=HARMONICS;

    cav->CosRight.endIndex=cav->ExpRight.startIndex;
    i=cav->CosRight.endIndex;
    while (sign(cav->Field[i])==sign(cav->Field[i-1]))
        i--;
    cav->CosRight.startIndex=i;

    numberOfData=cav->CosRight.endIndex-cav->CosRight.startIndex;

    cav->CosRight.frequency=1.0/(2.0*(cav->Position[cav->CosRight.endIndex]-cav->Position[cav->CosRight.startIndex]));
    cav->CosRight.phase=cav->Position[cav->CosRight.endIndex];

    x=gsl_matrix_alloc(numberOfData, cav->CosRight.numberOfHarmonics);
    fitCoefficients=gsl_vector_alloc(cav->CosRight.numberOfHarmonics);
    y=gsl_vector_alloc(numberOfData);
    covariant=gsl_matrix_alloc(cav->CosRight.numberOfHarmonics, cav->CosRight.numberOfHarmonics);
    workSpace=gsl_multifit_linear_alloc(numberOfData, cav->CosRight.numberOfHarmonics);

    for (i=0;i<numberOfData; i++)
    {
        gsl_vector_set(y, i, cav->Field[i+cav->CosRight.startIndex]);
        for (j=0;j<cav->CosRight.numberOfHarmonics; j++)
            gsl_matrix_set(x, i, j, cos((double)(2*j+1)*M_PI*cav->CosRight.frequency*(cav->Position[i+cav->CosRight.startIndex]-cav->CosRight.phase)));
    }

    gsl_multifit_linear(x, y, fitCoefficients, covariant, &chi, workSpace);

    cav->CosRight.amplitude=(double *)malloc(sizeof(double)*cav->CosRight.numberOfHarmonics);

    for (i=0;i<cav->CosRight.numberOfHarmonics;i++)
        cav->CosRight.amplitude[i]=gsl_vector_get(fitCoefficients, i);

    for (i=cav->CosRight.startIndex;i<cav->CosRight.endIndex;i++)
        cav->AnalyticField[i]=cav->CosRight.amplitude[0]*cos(M_PI*cav->CosRight.frequency*(cav->Position[i]-cav->CosRight.phase))+cav->CosRight.amplitude[1]*cos(3.0*M_PI*cav->CosRight.frequency*(cav->Position[i]-cav->CosRight.phase))+cav->CosRight.amplitude[2]*cos(5.0*M_PI*cav->CosRight.frequency*(cav->Position[i]-cav->CosRight.phase));

    gsl_vector_free(y);
    gsl_vector_free(fitCoefficients);
    gsl_matrix_free(x);
    gsl_matrix_free(covariant);
    gsl_multifit_linear_free(workSpace);
    return;
}

void sinusoidalFitCenter(cavity * cav)
{
    gsl_matrix *x, *covariant;  
    gsl_vector *y;
    gsl_multifit_linear_workspace *workSpace;
    gsl_vector *fitCoefficients;
    int numberOfzeroes;
    double zeroes[20];
    double chi;
    int numberOfData;
    int i,j;

    cav->CosCenter.numberOfHarmonics=HARMONICS;

    cav->CosCenter.startIndex=cav->CosLeft.endIndex;
    cav->CosCenter.endIndex=cav->CosRight.startIndex;

    numberOfData=cav->CosCenter.endIndex-cav->CosCenter.startIndex;

    numberOfzeroes=0;
    for (i=cav->CosCenter.startIndex; i<cav->CosCenter.endIndex; i++)
        if (sign(cav->Field[i])!=sign(cav->Field[i+1]))
        {
            zeroes[numberOfzeroes]=cav->Position[i+1]-cav->Field[i+1]*(cav->Position[i+1]-cav->Position[i])/(cav->Field[i+1]-cav->Field[i]);
            numberOfzeroes++;
        }

    cav->CosCenter.frequency=((double)(numberOfzeroes-1))/(zeroes[numberOfzeroes-1]-zeroes[0]);

    cav->CosCenter.phase=(cav->Position[cav->CosCenter.startIndex]+cav->Position[cav->CosCenter.endIndex])/2.0+((double)(numberOfzeroes%2))/((2.0)*cav->CosCenter.frequency);

    x=gsl_matrix_alloc(numberOfData, cav->CosCenter.numberOfHarmonics);
    fitCoefficients=gsl_vector_alloc(cav->CosCenter.numberOfHarmonics);
    y=gsl_vector_alloc(numberOfData);
    covariant=gsl_matrix_alloc(cav->CosCenter.numberOfHarmonics, cav->CosCenter.numberOfHarmonics);
    workSpace=gsl_multifit_linear_alloc(numberOfData, cav->CosCenter.numberOfHarmonics);

    for (i=0;i<numberOfData; i++)
    {
        gsl_vector_set(y, i, cav->Field[i+cav->CosCenter.startIndex]);
        for (j=0;j<cav->CosCenter.numberOfHarmonics; j++)
            gsl_matrix_set(x, i, j, cos((double)(2*j+1)*M_PI*cav->CosCenter.frequency*(cav->Position[i+cav->CosCenter.startIndex]-cav->CosCenter.phase)));
    }

    gsl_multifit_linear(x, y, fitCoefficients, covariant, &chi, workSpace);

    cav->CosCenter.amplitude=(double *)malloc(sizeof(double)*cav->CosCenter.numberOfHarmonics);

    for (i=0;i<cav->CosCenter.numberOfHarmonics;i++)
        cav->CosCenter.amplitude[i]=gsl_vector_get(fitCoefficients, i);

    for (i=cav->CosCenter.startIndex; i<cav->CosCenter.endIndex;i++)
        cav->AnalyticField[i]=cav->CosCenter.amplitude[0]*cos(M_PI*cav->CosCenter.frequency*(cav->Position[i]-cav->CosCenter.phase))+cav->CosCenter.amplitude[1]*cos(3.0*M_PI*cav->CosCenter.frequency*(cav->Position[i]-cav->CosCenter.phase))+cav->CosCenter.amplitude[2]*cos(5.0*M_PI*cav->CosCenter.frequency*(cav->Position[i]-cav->CosCenter.phase));

    gsl_vector_free(y);
    gsl_vector_free(fitCoefficients);
    gsl_matrix_free(x);
    gsl_matrix_free(covariant);
    gsl_multifit_linear_free(workSpace);
    return;
}

void Fit(cavity * cav)
{
    exponentialFitLeft(cav);
    exponentialFitRight(cav);
    sinusoidalFitLeft(cav);
    sinusoidalFitRight(cav);
    sinusoidalFitCenter(cav);
    return;
}
